FROM golang:1.11 AS builder

RUN mkdir /app
ADD *.go /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /app/envinfo .

# ------
FROM scratch
COPY --from=builder /app/envinfo /app/envinfo
EXPOSE 8080

ENTRYPOINT ["/app/envinfo"]
