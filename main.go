package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "[ Usage ]")
	fmt.Fprintln(w)
	fmt.Fprintln(w, "-> Display Usage (this!)")
	fmt.Fprintln(w, "http://localhost:8080/")
	fmt.Fprintln(w)
	fmt.Fprintln(w, "-> Display environment variables.")
	fmt.Fprintln(w, "http://localhost:8080/env")
	fmt.Fprintln(w)
	fmt.Fprintln(w, "-> Query DNS")
	fmt.Fprintln(w, "http://localhost:8080/dns?resolve=www.yahoo.com")
}

func envHandler(w http.ResponseWriter, r *http.Request) {

	for _, e := range os.Environ() {
		fmt.Fprintf(w, e+"\n")
	}
}

func dnsHandler(w http.ResponseWriter, r *http.Request) {
	toResolve := r.URL.Query().Get("resolve")
	if toResolve != "" {
		result, err := net.LookupHost(toResolve)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		} else {
			for _, ns := range result {
				fmt.Fprintf(w, "%s\n", ns)
			}
		}
	}
}

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/env", envHandler)
	http.HandleFunc("/dns", dnsHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
